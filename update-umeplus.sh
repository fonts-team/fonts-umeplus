#!/usr/bin/bash

MPLUSVER="063"
UMEVER="670"
UMEPLUSVER="20180604"

rm -rf mplus-TESTFLIGHT-${MPLUSVER}
tar xf mplus-TESTFLIGHT-${MPLUSVER}.tar.xz
rm -rf umefont_${UMEVER}
tar xf umefont_${UMEVER}.tar.xz
rm -rf umeplus-fonts-${UMEPLUSVER}
rm umeplus-fonts-${UMEPLUSVER}.tar.xz

rm umeplus-fonts-dev/docs-*/*
rm umeplus-fonts-dev/fontforge-scripts-umeplus*/*.ttf

cp update-umeplus.sh umeplus-fonts-dev/

cp mplus-TESTFLIGHT-${MPLUSVER}/mplus-1m-regular.ttf umeplus-fonts-dev/fontforge-scripts-umeplus/
cp mplus-TESTFLIGHT-${MPLUSVER}/mplus-1c-regular.ttf umeplus-fonts-dev/fontforge-scripts-umeplus-p/
cp mplus-TESTFLIGHT-${MPLUSVER}/{LICENSE_*,README_*} umeplus-fonts-dev/docs-mplus/

chmod 644 umefont_${UMEVER}/*
cp umefont_${UMEVER}/ume-tgo4.ttf umeplus-fonts-dev/fontforge-scripts-umeplus/
cp umefont_${UMEVER}/ume-pgo4.ttf umeplus-fonts-dev/fontforge-scripts-umeplus-p/
cp umefont_${UMEVER}/license.html umeplus-fonts-dev/docs-ume/

cd umeplus-fonts-dev/fontforge-scripts-umeplus/
fontforge -script fix-mplus-1m-regular.pe
fontforge -script import-mplus.pe
cd -

cd umeplus-fonts-dev/fontforge-scripts-umeplus-p/
fontforge -script fix-mplus-1c-regular.pe
fontforge -script import-mplus.pe
cd -

mv umeplus-fonts-dev/fontforge-scripts-umeplus*/umeplus-*.ttf umeplus-fonts-dev/
rm umeplus-fonts-dev/fontforge-scripts-umeplus*/*.ttf

mv umeplus-fonts-dev umeplus-fonts-${UMEPLUSVER}
tar Jcvf umeplus-fonts-${UMEPLUSVER}.tar.xz umeplus-fonts-${UMEPLUSVER}
mv umeplus-fonts-${UMEPLUSVER} umeplus-fonts-dev
